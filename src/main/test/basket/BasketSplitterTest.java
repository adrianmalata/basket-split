package basket;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class BasketSplitterTest {
    private final ObjectMapper objectMapper = new ObjectMapper();

    private final String pathToConfigFile = "src/main/resources/config.json";
    private final BasketSplitter basketSplitter = new BasketSplitter(pathToConfigFile);

    private final String pathToBasket1 = "src/main/resources/basket-1.json";
    private final String pathToBasket2 = "src/main/resources/basket-2.json";
    private final List<String> basket1 = readFromFile(new File(pathToBasket1));
    private final List<String> basket2 = readFromFile(new File(pathToBasket2));

    @Test
    void splitBasket1() {
        assert basket1 != null;
        Map<String, List<String>> splitBasket1 = basketSplitter.split(basket1);

        assertEquals(5, splitBasket1.get("Courier").size());
        assertEquals(1, splitBasket1.get("Express Collection").size());
        assertNull(splitBasket1.get("Same day delivery"));
    }

    @Test
    void splitBasket2() {
        assert basket2 != null;
        Map<String, List<String>> splitBasket2 = basketSplitter.split(basket2);

        assertEquals(13, splitBasket2.get("Express Collection").size());
        assertEquals(3, splitBasket2.get("Same day delivery").size());
        assertEquals(1, splitBasket2.get("Courier").size());
        assertNull(splitBasket2.get("Pick-up point"));
    }
    
    @Test
    void testEmptyBasket() {
        List<String> emptyBasket = Collections.emptyList();
        Map<String, List<String>> splitEmptyBasket = basketSplitter.split(emptyBasket);

        assertTrue(splitEmptyBasket.isEmpty());
    }

    @Test
    void testNullBasket() {
        Map<String, List<String>> splitNullBasket = basketSplitter.split(null);
        assertTrue(splitNullBasket.isEmpty());
    }

    private <T> T readFromFile(File file) {
        try {
            return objectMapper.readValue(file, new TypeReference<>() {});
        } catch (IOException e) {
            return null;
        }
    }
}