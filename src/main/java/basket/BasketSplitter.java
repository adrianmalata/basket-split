package basket;

import basket.utilities.ConfigFileReader;
import basket.utilities.OptimalDeliverySelector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

/**
 * This class is responsible for splitting logically related items into distinct delivery types.
 * It employs a configuration map to determine the categorization rules and applies them to provided item lists.
 * The primary method, split, performs the task of item categorization.
 * Internal methods are used to check the validity of provided items and facilitate the splitting process.
 */
public class BasketSplitter {

    private final Logger logger = LogManager.getLogger(this.getClass());
    private final Map<String, List<String>> configMap;

    /**
     * Constructor for the BasketSplitter class.
     *
     * @param absolutePathToConfigFile The absolute path to the configuration file
     * that contains the categorization rules for splitting the basket.
     */
    public BasketSplitter(String absolutePathToConfigFile) {
        configMap = new ConfigFileReader().readConfigFile(absolutePathToConfigFile);
    }

    /**
     * This method splits the list of items and maps them.
     *
     * @param items The list of items that needs to be split.
     * @return Returns a map where each delivery is mapped to a corresponding list of items.
     *         If the provided list of items is either null or empty, an empty map is returned.
     */
    public Map<String, List<String>> split(List<String> items) {
        Map<String, List<String>> result = new HashMap<>();

        boolean itemsIsNullOrEmpty = checkNullOrEmptyItems(items);

        if (itemsIsNullOrEmpty) {
            return result;
        } else {
            return splitBasket(items, result);
        }
    }

    private boolean checkNullOrEmptyItems(List<String> items) {
        if (items == null || items.isEmpty()) {
            logger.error("Empty or null list of items provided");
            return true;
        }
        return false;
    }

    private Map<String, List<String>> splitBasket(List<String> items, Map<String, List<String>> result) {
        logger.info("Splitting new basket, {} items", items.size());
        OptimalDeliverySelector selector = new OptimalDeliverySelector(configMap, items);

        for (String item : items) {
            String chosenDelivery = selector.getItemOptimalDelivery(item);
            addItemToDelivery(result, chosenDelivery, item);
        }

        logger.info("Items split successfully");
        return result;
    }

    private void addItemToDelivery(Map<String, List<String>> map, String delivery, String item) {
        map.computeIfAbsent(delivery, key -> new ArrayList<>()).add(item);
    }
}