## Basket Splitter Library

***

### Description:

This project contains an basket splitting system designed to optimize the process of managing items in a basket and ensuring 
their optimal delivery. Our primary goal is to make the process of online shopping more efficient and to deliver
your items in the most cost-effective manner.

#### Key features:
* **Basket Splitter**: Our system includes a Basket Splitter class that smartly divides the items in a basket based on particular configurations.
* **Optimal Delivery Selector**: For every item in the basket, this class helps in choosing the optimal delivery method, considering different factors to ensure efficient delivery to the end user.
* **Error Handling & Logging**: All critical operations are tested for errors and appropriate logs are captured for debugging purposes.

Please refer to the individual classes for a more in-depth understanding of the approach used. Contributions and suggestions are welcome!

***

### Setup guide:

1. **Importing the library** - Download the JAR file of the library and include it in your classpath. In case of a Maven project, add the JAR file to the lib directory in your project and then add a system scoped dependency:

    ```
   <dependency>
        <groupId>com.yourgroup</groupId>
        <artifactId>basket-splitter</artifactId>
        <version>1.0.0</version>
        <scope>system</scope>
        <systemPath>${project.basedir}/lib/basket-splitter-1.0.0.jar</systemPath>
    </dependency>
   ```
   Where "yourgroup" should be replaced by your group id, e-commerce-basket-management-lib with the name of the downloaded JAR file, and 1.0.0 with the version of the JAR.


2. **Setting up Logger** - Our library uses Log4j for logging. To set it up, add a log4j.properties or log4j.xml file to your resources folder. Adjust its configurations according to your needs.

***

### Using the library:

1. **Creating an instance of BasketSplitter** - To use this class, you need to create an instance of it, passing the appropriate parameter *path* of type `String`.

   ```
    //load your config file absolute path
    String absolutePathToConfigFile = ...;

    BasketSplitter basketSplitter = new BasketSplitter(absolutePathToConfigFile);
   ```

2. **Invoking method** - Once the object has been created, you can invoke the required `split()` method, passing the appropriate parameter *items* of type `List<String>`.

   ```
    //load your basket to items
    List<String> items = ...;
    basketSplitter.split(items);
   ```

This guide should help you get started with the Basket Splitter Library. For any further queries or suggestions, feel free to contact us!

***

### Running Tests:

This project uses JUnit for testing. To run the tests:

1. **Via an IDE**: If you're using an IDE like IntelliJ IDEA or Eclipse, you can right-click on the test directory and click on Run 'All Tests'.
2. **Via Command Line**: If you're good with the command line, you can navigate to the project root directory and run the following Maven command: `mvn test`
   
This command will find all test classes and run them.

***

### Libraries:

**Tests:** JUnit Jupiter 5.10.2

**JSON parsing:** Jackson 2.16.1

**Logging:** Log4j 2.22.1