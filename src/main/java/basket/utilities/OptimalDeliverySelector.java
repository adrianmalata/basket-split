package basket.utilities;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class OptimalDeliverySelector {
    private final Logger logger = LogManager.getLogger(this.getClass());
    private final Map<String, List<String>> configMap;
    private final List<String> allDeliveries;

    public OptimalDeliverySelector(Map<String, List<String>> configMap, List<String> items) {
        this.configMap = configMap;
        allDeliveries = getAllDeliveries(items);
    }

    public String getItemOptimalDelivery(String item) {
        List<String> availableDeliveries = getAvailableDeliveries(item);
        return getMostFrequentDelivery(availableDeliveries, allDeliveries);
    }

    private List<String> getAvailableDeliveries(String item) {
        List<String> availableDeliveries;

        if (configMap.containsKey(item)) {
            availableDeliveries = configMap.get(item);
            logger.info("Got available deliveries for item: {}", item);
        } else {
            availableDeliveries = Collections.emptyList();
            logger.error("No available deliveries defined for item: {}", item);
        }

        return availableDeliveries;
    }

    private List<String> getAllDeliveries(List<String> items) {
        List<String> deliveries = items.stream()
                                       .flatMap(this::getDeliveryOptions)
                                       .toList();

        if (!deliveries.isEmpty()) logger.info("Got all possible deliveries");

        return deliveries;
    }

    private Stream<String> getDeliveryOptions(String item) {
        try {
            return configMap.get(item).stream();
        } catch (NullPointerException nullPointerException) {
            logger.error("No deliveries defined for item: {}", item);
            return Stream.empty();
        }
    }

    private String getMostFrequentDelivery(List<String> availableDeliveries, List<String> allDeliveries) {
        String chosenDelivery = "";
        int maxCount = 0;

        if (availableDeliveries == null || availableDeliveries.isEmpty() || allDeliveries == null || allDeliveries.isEmpty()) {
            return "Not recognized products";
        }

        for (String delivery : availableDeliveries) {
            int count = Collections.frequency(allDeliveries, delivery);

            if (count > maxCount) {
                maxCount = count;
                chosenDelivery = delivery;
            }
        }

        logger.info("Selected delivery: {}", chosenDelivery);
        return chosenDelivery;
    }
}