package basket.utilities;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class OptimalDeliverySelectorTest {
    private final String pathToConfigFile = "src/main/resources/config.json";
    private final Map<String, List<String>> configMap = new ConfigFileReader().readConfigFile(pathToConfigFile);

    @Test
    void testProductWithoutDelivery() {
        List<String> testBasket = Collections.singletonList("Random product");
        OptimalDeliverySelector selector = new OptimalDeliverySelector(configMap, testBasket);
        String itemOptimalDelivery = selector.getItemOptimalDelivery(testBasket.getFirst());

        assertEquals("Not recognized products", itemOptimalDelivery);
    }

    @Test
    void testMixedProductsWithAndWithoutDelivery() {
        List<String> mixedBasket = Arrays.asList("Random product", "Fond - Chocolate");
        OptimalDeliverySelector selector = new OptimalDeliverySelector(configMap, mixedBasket);

        String deliveryForRandom = selector.getItemOptimalDelivery(mixedBasket.getFirst());
        assertEquals("Not recognized products", deliveryForRandom);

        String deliveryForFond = selector.getItemOptimalDelivery(mixedBasket.getLast());
        assertEquals("Pick-up point", deliveryForFond);
    }

}