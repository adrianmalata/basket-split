package basket.utilities;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class ConfigFileReaderTest {

    @Test
    void readValidConfigFile() {
        String path = "src/main/resources/config.json";
        ConfigFileReader reader = new ConfigFileReader();
        Map<String, List<String>> configFile = reader.readConfigFile(path);

        assertFalse(configFile.isEmpty());
    }

    @Test
    void readNotExistingConfigFile() {
        String path = "src/main/resources/invalid_config.json";
        ConfigFileReader reader = new ConfigFileReader();
        Map<String, List<String>> configFile = reader.readConfigFile(path);

        assertTrue(configFile.isEmpty());
    }

    @Test
    void readInvalidFormatConfigFile() {
        String path = "src/main/resources/basket-1.json";
        ConfigFileReader reader = new ConfigFileReader();
        Map<String, List<String>> configFile = reader.readConfigFile(path);

        assertTrue(configFile.isEmpty());
    }

}