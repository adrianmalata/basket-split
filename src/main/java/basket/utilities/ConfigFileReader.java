package basket.utilities;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConfigFileReader {
    private final Logger logger = LogManager.getLogger(this.getClass());

    public Map<String, List<String>> readConfigFile(String absolutePathToConfigFile) {
        File configFile = new File(absolutePathToConfigFile);
        Map<String, List<String>> map = new HashMap<>();

        try {
            map = new ObjectMapper().readValue(configFile, new TypeReference<>(){});
            logger.info("Config file loaded successfully");
        } catch (FileNotFoundException fileNotFoundException) {
            logger.error("Config file not found at: \"{}\"", absolutePathToConfigFile);
        } catch (IOException ioException) {
            logger.error("Error reading and parsing config file \"{}\" to Map<String, List<String>>", absolutePathToConfigFile);
        }

        return map;
    }
}